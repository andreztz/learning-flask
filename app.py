from flask import Flask

import db
import admin
import contact
import views


def create_app():
    app = Flask(__name__)

    db.configure(app)
    admin.configure(app)
    contact.configure(app)
    views.configure(app)
    # config extensions

    # config variables
    app.config['SECRET_KEY'] = 'kahsfownvvhquradsda'
    return app
