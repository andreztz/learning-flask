from flask import abort
from flask import Blueprint
from flask import current_app
from flask import request
from flask import render_template


bp = Blueprint(
    'contact',
    __name__,
    url_prefix='/contact'
    )

@bp.route('/', methods=['GET', 'POST'])
def contact():
    # estão pedindo o form com metodo GET
    if request.method == 'GET':
        return render_template('contact.html')
    # processar dados
    name = request.form.get('name')
    message = request.form.get('message')
    # validar
    if not name or not message:
        abort(400, 'invalid message')

    # banco de dados
    current_app.db.messages.insert_one({'name': name, 'message': message})

    return 'succsess!!'


def configure(app):
    app.register_blueprint(bp)
