from flask import jsonify
from flask import render_template


def configure(app):

    @app.route('/')
    def index():
        return 'Olá Mundo'

    @app.route('/langs')
    def langs():
        return render_template(
            'index.html',
            title='Melhores Linguagens de Programação',
            languages=['Python', 'JavaScript']
        )

    @app.route('/api')
    def api():
        return jsonify(data={'name': 'Andre'})
